﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 14.04.2015
 * Time: 11:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using WinCCReportCreator;
using System.Text.RegularExpressions;

namespace VodokanalReporter
{
	/// <summary>
	/// Description of ExcelWriterEquipment.
	/// </summary>
	public class ExcelWriterEquipment : ExcelWriterBase
	{
		private int _graphIndex = 0;
		private int _dayValuesCount = 1; //для запоминания количества точек, полученных за определенный день
		private bool _isDayEnded = false; //признак того, что день кончился и можно начинать группировку, вставлять формулы и графики
		private bool _isFirstDay = true; //признак того, что это первый день для параметра на листе
		private DateTime _previousDate;
		private int _offsetRow = 0; 
		private int _offsetColumn = 0;
		private string _formula = "";
		
		private List<string> _pumps = null;
		
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="objName">Имя объекта, для которого строится отчет</param>
		/// <param name="listClass">Объект, реализующий интерфейс IListClass, предоставляющий список точек системы</param>
		/// <param name="taskClass">Объект, реализующий интерфейс ITaskClass, предоставляющий задание для построения отчета</param>
		/// <param name="winCC">Объект, реализующий интерфейс IWinCCOleDBReader, предоставляющий доступ к базе данных WinCC</param>
		public ExcelWriterEquipment(string objName, IListClass listClass, ITaskClass taskClass, IWinCCOleDBReader winCC) :
			base(objName, listClass, taskClass, winCC)
		{
			_pumps = new List<string>();
			GetPumps();
		}
		
		//КИР ВОС. Насос N5 температура 1а
		
		/// <summary>
		/// Для получения списка насосов всех точек
		/// </summary>
		private void GetPumps()
		{
			string label = "";
			string pumpName = "";
			foreach (var point in _points)
			{
				label = point.Label;
				//.*?\s(Насос\s.*?\sтемпература)
				//это не интересующая нас последовательность символов
				if (!Regex.IsMatch(label, @".*?\sНасос\s.*?\sтемпература", RegexOptions.IgnoreCase | RegexOptions.Singleline))
					continue;
				
				pumpName = Regex.Match(label, @".*?\s(Насос\s.*?\sтемпература)", RegexOptions.IgnoreCase | RegexOptions.Singleline).Result("$1");
				_pumps.Add(pumpName);
			}
			
			//убираем дубликаты
			_pumps = _pumps.Distinct().ToList();
		}
		
		/// <summary>
		/// Создает страницы по работе насосов
		/// </summary>
		public void CreatePumpPages()
		{
			foreach (var pump in _pumps)
			{
				_graphIndex = 0;
				_dayValuesCount = 1; //для запоминания количества точек, полученных за определенный день
				_isDayEnded = false; //признак того, что день кончился и можно начинать группировку, вставлять формулы и графики
				_isFirstDay = true; //признак того, что это первый день для параметра на листе
				_previousDate = new DateTime(2000, 1, 1);
				_offsetRow = 0; 
				_offsetColumn = 0;
				_formula = "";
				ExcelWorksheet worksheet = _Excel.Workbook.Worksheets.Add(pump);
				foreach(var point in _points)
				{
					if (point.Label.Contains(pump)) //если нашли точку, соответсвующую насосу
					{
						CreatePumpPage(worksheet, point);
					}
				}
			}
		}
		
		/// <summary>
		/// Заполняет лист данными по насосу
		/// </summary>
		/// <param name="worksheet">Ссылка на лист, который нужно заполнить</param>
		/// <param name="point">Точка, при помощи которой производится заполнение</param>
		private void CreatePumpPage(ExcelWorksheet worksheet, PointClass point)
		{
			TagHistoricalDataClass hdc;
				
			hdc = _winCC.ReadTagFromDB(
				"Archive",
				point.TagName + "-value",
				_taskClass.StartDate,
				_taskClass.EndDate, "",
				3600,
				TimeStepType.Average);
			
			//TODO убрать, для теста
//					hdc = new TagHistoricalDataClass();
//					hdc.AddValue(1.0, DateTime.Now, 1, 1);
//					hdc.AddValue(2.0, DateTime.Now.AddMinutes(1), 2, 1);
//					hdc.AddValue(3.0, DateTime.Now.AddMinutes(2), 3, 1);
//					hdc.AddValue(3.2, DateTime.Now.AddMinutes(3), 3, 1);
//					hdc.AddValue(3.1, DateTime.Now.AddMinutes(4), 3, 1);
//					hdc.AddValue(3.3, DateTime.Now.AddMinutes(6), 3, 1);
//					hdc.AddValue(3.44, DateTime.Now.AddMinutes(8), 3, 1);
//					hdc.AddValue(1.5, DateTime.Now.AddMinutes(10), 3, 1);
//
//					hdc.AddValue(4.0, DateTime.Now.AddDays(-1), 1, 1);
//					//hdc.AddValue(5.0, DateTime.Now.AddMinutes(1).AddDays(-1), 2, 1);
//					//hdc.AddValue(6.0, DateTime.Now.AddMinutes(2).AddDays(-1), 3, 1);
//
//					hdc.AddValue(7.0, DateTime.Now.AddDays(-2), 1, 1);
//					hdc.AddValue(8.0, DateTime.Now.AddMinutes(1).AddDays(-2), 2, 1);
//					hdc.AddValue(9.1, DateTime.Now.AddMinutes(2).AddDays(-2), 3, 1);
//					hdc.AddValue(9.2, DateTime.Now.AddMinutes(3).AddDays(-2), 3, 1);
//					hdc.AddValue(9.3, DateTime.Now.AddMinutes(4).AddDays(-2), 3, 1);
//					hdc.AddValue(9.4, DateTime.Now.AddMinutes(5).AddDays(-2), 3, 1);
			
			_previousDate = new DateTime(2000, 1, 1); //начальная дата, которой точно не будет в базе
			
			worksheet.Cells["A1"].Offset(_offsetRow, _offsetColumn).Value = point.Code;
			worksheet.Cells["B1"].Offset(_offsetRow, _offsetColumn).Value = point.Label;
			worksheet.Cells["D1"].Offset(_offsetRow, _offsetColumn).Value = point.Unit;
			
			worksheet.Cells["A1:D1"].Offset(_offsetRow, _offsetColumn).Style.Font.Bold = true;
			
			_isDayEnded = false;
			_isFirstDay = true;
			
			foreach (var val in hdc.TagValues)
			{
				if (val.TagTimeStamp.Date == _previousDate.Date) //если день еще не кончился
				{
					worksheet.Cells["C2"].Offset(_offsetRow, _offsetColumn).Value = val.TagTimeStamp.ToString("dd.MM.yyyy HH:mm");
					worksheet.Cells["D2"].Offset(_offsetRow, _offsetColumn).Value = val.TagValue;
					worksheet.Cells["D2"].Offset(_offsetRow, _offsetColumn).Style.Numberformat.Format =  "0.00";
					_dayValuesCount++;
				}
				else //наступил следующий день
				{
					worksheet.Cells["C2"].Offset(_offsetRow, _offsetColumn).Value = val.TagTimeStamp.ToString("dd.MM.yyyy");
					worksheet.Cells["C2:D2"].Offset(_offsetRow, _offsetColumn).Style.Font.Bold = true;
					worksheet.Cells["C2:D2"].Offset(_offsetRow, _offsetColumn).Style.Font.Italic = true;
					_offsetRow++;
					worksheet.Cells["C2"].Offset(_offsetRow, _offsetColumn).Value = val.TagTimeStamp.ToString("dd.MM.yyyy HH:mm");
					worksheet.Cells["D2"].Offset(_offsetRow, _offsetColumn).Value = val.TagValue;
					worksheet.Cells["D2"].Offset(_offsetRow, _offsetColumn).Style.Numberformat.Format =  "0.00";
					
					if (_isFirstDay)
						_isFirstDay = false;
					else
						_isDayEnded = true;
				}
				
				if (_isDayEnded) //строим графики, группируем, вставляем формулы
				{
					//формула суммы
					_formula = String.Format("(R{0}C{1}:R{2}C{1})",
					                        _offsetRow - _dayValuesCount + 1,
					                        4 + _offsetColumn,
					                        _offsetRow);
					worksheet.Cells["D2"].Offset(_offsetRow - _dayValuesCount - 2, _offsetColumn).FormulaR1C1 = "=AVERAGE" + _formula; //формула для подсчета суммы
					worksheet.Cells["D2"].Offset(_offsetRow - _dayValuesCount - 2, _offsetColumn).Style.Numberformat.Format =  "0.00";
					
					//группировка
					for (int i = _offsetRow - _dayValuesCount + 1; i <= _offsetRow; i++)
						worksheet.Row(i).OutlineLevel = 1;
					
					//графики
					if (_dayValuesCount > 5)
					{
						ExcelChart ec = worksheet.Drawings.AddChart("Line Time Graph Eq" + _graphIndex.ToString(), OfficeOpenXml.Drawing.Chart.eChartType.Line);
						_graphIndex++;
						ec.Title.Text = "Часовые показания";
						ec.SetPosition(_offsetRow - _dayValuesCount, 0, _offsetColumn + 5, 0);
						ec.SetSize(320, 20 * _dayValuesCount);
						ec.Legend.Remove();
						
						string timeRangeAdr  = String.Format("C{0}:C{1}", _offsetRow - _dayValuesCount + 1, _offsetRow);
						string valueRangeAdr = String.Format("D{0}:D{1}", _offsetRow - _dayValuesCount + 1, _offsetRow);
						
						var timeRange  = worksheet.Cells[timeRangeAdr].Offset(0, _offsetColumn);
						var valueRange = worksheet.Cells[valueRangeAdr].Offset(0, _offsetColumn);
						
						var serie = ec.Series.Add(valueRange, timeRange);
					}
					
					_dayValuesCount = 1;
					_isDayEnded = false;
				}
				
				_previousDate = val.TagTimeStamp;
				_offsetRow++;
			}
			
			//Для того, чтобы последний день показать
			//***************************************
			//формула суммы
			_formula = String.Format("(R{0}C{1}:R{2}C{1})",
			                        _offsetRow - _dayValuesCount + 2,
			                        4 + _offsetColumn,
			                        _offsetRow + 1);
			worksheet.Cells["D1"].Offset(_offsetRow - _dayValuesCount, _offsetColumn).FormulaR1C1 = "=AVERAGE" + _formula; //формула для подсчета суммы
			worksheet.Cells["D1"].Offset(_offsetRow - _dayValuesCount, _offsetColumn).Style.Numberformat.Format =  "0.00";
			
			//группировка
			for (int i = _offsetRow - _dayValuesCount + 2; i <= _offsetRow + 1; i++)
				worksheet.Row(i).OutlineLevel = 1;
			
			//графики
			if (_dayValuesCount > 5)
			{
				ExcelChart ec = worksheet.Drawings.AddChart("Line Time Graph Eq" + _graphIndex.ToString(), OfficeOpenXml.Drawing.Chart.eChartType.Line);
				_graphIndex++;
				ec.Title.Text = "Часовые показания";
				ec.SetPosition(_offsetRow - _dayValuesCount + 1, 0, _offsetColumn + 5, 0);
				ec.SetSize(320, 20 * _dayValuesCount);
				ec.Legend.Remove();
				
				string timeRangeAdr  = String.Format("C{0}:C{1}", _offsetRow - _dayValuesCount + 2, _offsetRow + 1);
				string valueRangeAdr = String.Format("D{0}:D{1}", _offsetRow - _dayValuesCount + 2, _offsetRow + 1);
				
				var timeRange  = worksheet.Cells[timeRangeAdr].Offset(0, _offsetColumn);
				var valueRange = worksheet.Cells[valueRangeAdr].Offset(0, _offsetColumn);
				
				var serie = ec.Series.Add(valueRange, timeRange);
			}
			//***************************************
			
			worksheet.Column(1 + _offsetColumn).AutoFit();
			worksheet.Column(2 + _offsetColumn).AutoFit();
			worksheet.Column(3 + _offsetColumn).AutoFit();
			worksheet.Column(4 + _offsetColumn).AutoFit();
			worksheet.Column(5 + _offsetColumn).AutoFit();
			
			_dayValuesCount = 1;
			_offsetRow = 0;
			_offsetColumn += 10;
		}
		
		
		
	}
}
