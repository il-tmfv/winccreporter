﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 20.02.2015
 * Time: 10:28
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using WinCCReportCreator;
using System.IO;
using System.Collections.Generic;

namespace VodokanalReporter
{	
	public enum ResourceType 
	{
		Pressure,
		Flow,
		Electro,
		Temperature
	}
		
	/// <summary>
	/// Тип требуемого отчета
	/// </summary>
	public enum ReportType
	{
		Undefined = 0, //Тип отчета не определен
		Resources = 1, //Отчет по ресурсам
		Equipment = 2  //Отчет по работе оборудования
	}
	
	/// <summary>
	/// Класс, реализующий всю логику работы приложения
	/// </summary>
	public class ManagerClass
	{
		private IMessageClass _messageClass;
		private IListClass _listClass;
		private ITaskClass _taskClass;
		private IWinCCOleDBReader _winCC;
		
		/// <summary>
		/// Создание отчета по оборудованию
		/// </summary>
		private void CreateReportEquipment()
		{
			//цикл по всем объектам, для которых нужно сгенерировать отчет
			foreach (var objName in _taskClass.Places)
			{
				//создаем отчетный файл
				ExcelWriterEquipment ew = new ExcelWriterEquipment(objName, _listClass, _taskClass, _winCC);
				
				ew.CreatePumpPages();
				
				ew.Save();		
			}
		}
		
		/// <summary>
		/// Создание отчета по ресурсам 
		/// </summary>
		private void CreateReportResource()
		{
			//цикл по всем объектам, для которых нужно сгенерировать отчет
			foreach (var objName in _taskClass.Places)
			{
				//создаем отчетный файл
				ExcelWriterResources ew = new ExcelWriterResources(objName, _listClass, _taskClass, _winCC);
				
				ew.CreateSumPageReportResource();
				ew.CreatePageReportRecource(RecourceTypeToString(ResourceType.Flow), "Вода-Расход");
				ew.CreatePageReportRecource(RecourceTypeToString(ResourceType.Pressure), "Вода-Давление");
				ew.CreatePageReportRecource(RecourceTypeToString(ResourceType.Electro), "Электроэнергия");					
				
				ew.Save();		
			}
		}
		
		/// <summary>
		/// Преобразует тип ресурса в строку "E", "F", "T" или "P"
		/// </summary>
		/// <param name="rs">Тип ресурса, заданный через перечисление ResourceType</param>
		/// <returns>Строка "E", "F", "T" или "P"</returns>
		private string RecourceTypeToString(ResourceType rs)
		{
			string result = "";
			switch (rs)
			{
				case ResourceType.Pressure:
					result = "P";
					break;
				case ResourceType.Electro:
					result = "E";
					break;
				case ResourceType.Temperature:
					result = "T";
					break;
				case ResourceType.Flow:
					result = "F";
					break;
			}
			return result;
		}
		
		/// <summary>
		/// Подключение к базе данных WinCC
		/// </summary>
		/// <returns></returns>
		private bool ConnectToWinCC()
		{
			_winCC.OpenConnection(_taskClass.CatalogName);
			return _winCC.IsConnected;
		}
		
		/// <summary>
		/// Инициализация создания отчета на основе полученного задания
		/// </summary>
		private void CreateReport()
		{
			if (_taskClass.TypeOfReport == ReportType.Resources)
				CreateReportResource();
			else if (_taskClass.TypeOfReport == ReportType.Equipment)
				CreateReportEquipment();
			else
			{
				_messageClass.MessageError("Некорректный тип отчета = " + _taskClass.TypeOfReport);
				return;
			}		
		}
		
		/// <summary>
		/// Конструктор управляющего класса
		/// </summary>
		/// <param name="winCC">Объект для чтения данных из базы WinCC</param>
		/// <param name="taskClass">Объект с заданием для создания отчета</param>
		/// <param name="listClass">Объект "библиотека" со всеми точками системы</param>
		/// <param name="messageClass">Объект для наладки связи с пользователем (логи, консоль, ...)</param>
		public ManagerClass(IWinCCOleDBReader winCC, ITaskClass taskClass, IListClass listClass, IMessageClass messageClass)
		{
			_messageClass = messageClass;
			_listClass = listClass;
			_taskClass = taskClass;
			_winCC = winCC;
			
			//логируем задание
			_messageClass.MessageSuccess(_taskClass.ToString());
			
			//Проверяем корректность данных
			if (!_taskClass.IsCorrect)
			{
				_messageClass.MessageError("Некорректные параметры запуска");
				return;
			}
			ConnectToWinCC();
			CreateReport();
		}		
	}
}
