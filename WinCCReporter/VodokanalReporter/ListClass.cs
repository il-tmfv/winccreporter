﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 20.02.2015
 * Time: 8:17
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VodokanalReporter
{
	public interface IListClass
	{
		PointClass[] GetPointsByPlace(string place);
	}
	
	/// <summary>
	/// Класс для хранения точек у каждого объекта, хранения организовано в List'ах
	/// </summary>
	public class ListClass : IListClass
	{
		private List<PointClass> _listOfAllPoints = null; //Список всех точек
		
		/// <summary>
		/// Возращает точки, принадлежащие конкретному объекту
		/// </summary>
		/// <param name="place">Название объекта</param>
		/// <returns>Возвращает массив точк типа PointClass</returns>
		public PointClass[] GetPointsByPlace(string place)
		{
			var res = from n in _listOfAllPoints
						where n.Place == place
						select n;
			
			return res.ToArray();
		}
		
		/// <summary>
		/// Чтение всех точек из файла
		/// </summary>
		private void ReadAllPointsFromFile()
		{
			string[] tempStr = File.ReadAllLines("AllPoints.txt");
			
			foreach (string str in tempStr)
			{
				if (str.Length <= 0)
					continue;
				
				string[] pointParams = str.Split('\t');
				
				string tagName 	= pointParams[0];
				string code 	= pointParams[1];
				string label 	= pointParams[2];
				string unit 	= pointParams[3];
				string place 	= pointParams[4];
				bool isTotal 	= (pointParams[5] == "1") ? true : false;
				
				PointClass newPoint = new PointClass(tagName, code, label, unit, place, isTotal);
				
				_listOfAllPoints.Add(newPoint);
			}
		}
		
		public override string ToString()
		{
			return String.Format("Прочитан список параметров системы: {0} штук", _listOfAllPoints.Count);
		}
	
		/// <summary>
		/// Конструктор
		/// </summary>
		public ListClass()
		{
			_listOfAllPoints = new List<PointClass>();
			ReadAllPointsFromFile();
		}
	}
}
