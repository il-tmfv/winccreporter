﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 09.04.2015
 * Time: 15:58
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using WinCCReportCreator;
using System.Globalization;

namespace VodokanalReporter
{
	/// <summary>
	/// Description of ExcelWriterBase.
	/// </summary>
	public class ExcelWriterBase
	{
		protected ExcelPackage _Excel;
		protected PointClass[] _points;
		
		protected readonly IListClass _listClass;
		protected readonly ITaskClass _taskClass;
		protected readonly IWinCCOleDBReader _winCC;

		public IListClass ListClass
		{
			get { return _listClass; }
		}

		public ITaskClass TaskClass
		{
			get { return _taskClass; }
		}

		public IWinCCOleDBReader WinCC
		{
			get { return _winCC; }
		}
		
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="objName">Имя объекта, для которого строится отчет</param>
		/// <param name="listClass">Объект, реализующий интерфейс IListClass, предоставляющий список точек системы</param>
		/// <param name="taskClass">Объект, реализующий интерфейс ITaskClass, предоставляющий задание для построения отчета</param>
		/// <param name="winCC">Объект, реализующий интерфейс IWinCCOleDBReader, предоставляющий доступ к базе данных WinCC</param>
		public ExcelWriterBase(string objName, IListClass listClass, ITaskClass taskClass, IWinCCOleDBReader winCC)
		{
			_listClass 	= listClass;
			_taskClass 	= taskClass;
			_winCC 		= winCC;
			
			_Excel 		= CreateNewFile(taskClass.TypeOfReport, objName);
			_points 	= _listClass.GetPointsByPlace(objName);
		}
		
		public void Save()
		{
			_Excel.Save();
		}
		
		/// <summary>
		/// Метод для приведения даты и времени в компактный вид
		/// </summary>
		/// <param name="dt">Дата и время</param>
		/// <returns>Компактное представление даты и времени</returns>
		protected string CompactDateTime(DateTime dt)
		{
			return dt.ToString("yyyyMMddHHmmss");
		}
		
		/// <summary>
		/// Генерация имени для файла на основе типа отчета и названия участка
		/// </summary>
		/// <param name="reportType">Тип отчета</param>
		/// <param name="objName">Название объекта, для которого пишется отчет</param>
		protected string GetFileNameForReport(ReportType reportType, string objName)
		{
			string res = "";
			string resType = "";
			string workDir = @"C:\reports\";
			
			if (!Directory.Exists(workDir + objName))
				Directory.CreateDirectory(workDir + objName);
			
			if (reportType == ReportType.Resources)
				resType = "Учет";
			
			if (reportType == ReportType.Equipment)
				resType = "Оборудование";
			
			res = String.Format("{4}{0}\\{3}_{0}_{1}_{2}.xlsx",
			                    objName,
			                    CompactDateTime(_taskClass.StartDate),
			                    CompactDateTime(_taskClass.EndDate),
			                    resType,
			                    workDir);
			
			return res;
		}
		
		/// <summary>
		/// Для создания нового файла отчета
		/// </summary>
		/// <param name="reportType">Тип отчета</param>
		/// <param name="objName">Название объекта, для которого пишется отчет</param>
		/// <returns></returns>
		protected ExcelPackage CreateNewFile(ReportType reportType, string objName)
		{
			string fileName = GetFileNameForReport(reportType, objName);
			
			FileInfo newFile = new FileInfo(fileName);
			
			if (newFile.Exists)
			{
				newFile.Delete();  // ensures we create a new workbook
				newFile = new FileInfo(fileName);
			}
			
			return new ExcelPackage(newFile);
		}
	}
}
