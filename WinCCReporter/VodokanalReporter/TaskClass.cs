﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 20.02.2015
 * Time: 11:49
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace VodokanalReporter
{
	public interface ITaskClass
	{
		string UserName { get; }
		string CatalogName { get; }
		DateTime StartDate { get; }
		DateTime EndDate { get; }
		ReportType TypeOfReport { get; }
		string[] Places { get; }	
		bool IsCorrect { get; }
	}
	
	/// <summary>
	/// Класс содержит в себе описание задачи, поставленной перед генератором
	/// </summary>
	public class TaskClass : ITaskClass
	{
		private string _catalogName; 		//имя каталога WinCC
		DateTime _startDate;			//время начала генерации отчета
		DateTime _endDate;			//время конца генерации отчета
		private ReportType _typeOfReport; 	//тип отчета
		private string[] _places; 			//имена объектов, по которым надо сгенерировать отчет
		private string _userName;
		private bool _isCorrect;
		
		public bool IsCorrect
		{
			get
			{
				return _isCorrect;
			}
		}
		
		public string UserName 
		{
			get { return _userName; }
			private set { _userName = value; }
		}
		
		public string CatalogName 
		{
			get { return _catalogName; }
			private set { _catalogName = value; }
		}

		public DateTime StartDate 
		{
			get { return _startDate; }
			private set { _startDate = value; }
		}

		public DateTime EndDate 
		{
			get { return _endDate; }
			private set { _endDate = value; }
		}

		public ReportType TypeOfReport 
		{
			get { return _typeOfReport; }
			private set { _typeOfReport = value; }
		}

		public string[] Places 
		{
			get { return _places; }
			private set { _places = value; }
		}
		
		/// <summary>
		/// Получает участки, для которых необходимо сгенерировать отчет
		/// </summary>
		/// <param name="arg">Аргумент входящей строки, в котором записаны интересующие объекты</param>
		/// <returns>Массив с интересующими объектами</returns>
		private string[] GetPlaces(string arg)
		{
			int res = 0;
			List<string> listRes = new List<string>();
			
			if (Int32.TryParse(arg, out res) == false)
				return null;
			else
			{
				if ((res & 1) != 0)
					listRes.Add("КИР НС1");
				if ((res & 2) != 0)
					listRes.Add("КИР НС3");
				if ((res & 4) != 0)
					listRes.Add("КИР ВОС");
				if ((res & 8) != 0)
					listRes.Add("КОР НС1");
				if ((res & 16) != 0)
					listRes.Add("КОР ВОС");
				if ((res & 32) != 0)
					listRes.Add("Латошинка НС1");
				if ((res & 64) != 0)
					listRes.Add("Латошинка НС2");
				if ((res & 128) != 0)
					listRes.Add("Латошинка ВОС");
				if ((res & 256) != 0)
					listRes.Add("Олимпийская НС1");
				if ((res & 512) != 0)
					listRes.Add("Татьянка ВОС");
				if ((res & 1024) != 0)
					listRes.Add("ВОС ТЗР");
				if ((res & 2048) != 0) //для теста
					listRes.Add("ТЕСТ");
			}
			
			return listRes.ToArray();
		}
		
		/// <summary>
		/// Получает тип необходимого отчета из аргументов входящей строки
		/// </summary>
		/// <param name="arg">Аргумент входящей строки, в котором содержится тип отчета (1 или 2)</param>
		/// <returns>Возвращает тип требуемого отчета</returns>
		private ReportType GetReportType(string arg)
		{
			int res = 0;
			if (Int32.TryParse(arg, out res))
				return (ReportType)res;
			else
				return (ReportType)0;
		}
		
		public override string ToString()
		{
			if (_isCorrect)
				return String.Format("Получено задание от пользователя \"{0}\" на генерацию отчета типа \"{1}\" за период с {2} по {3} для {4} объектов: {5}",
			    	                 UserName, 
			    	                 TypeOfReport.ToString("g"), 
			    	                 StartDate, 
			    	                 EndDate, 
			    	                 (Places != null) ? Places.Length : 0,
			    	                 (Places != null) ? String.Join(", ", Places) : "---"
			    	                 );
			else
				return "Задание некорректно";
		}
		
		/// <summary>
		/// Конструктор класса, содержащего в себе задание на генерацию отчета.
		/// </summary>
		/// <param name="args">Нужно передать аргументы строки запуска приложения</param>
		public TaskClass(string[] args)
		{
			//небольшая проверка на корректность передаваемых данных
			if (args.Length < 6)
			{
				_isCorrect = false;
				return;
			}
			else 
				_isCorrect = true;
			
			CatalogName 	= args[0];
			StartDate 		= Convert.ToDateTime(args[1]);
			EndDate 		= Convert.ToDateTime(args[2]);
			TypeOfReport 	= GetReportType(args[3]);			
			Places 			= GetPlaces(args[4]);
			UserName 		= args[5];
			
			//если нет мест, то не получится сгенерировать
			if (Places.Length <= 0)
				_isCorrect = false;
		}
		
	}
}
