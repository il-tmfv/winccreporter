﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 12.01.2015
 * Time: 14:52
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using WinCCReportCreator;
using System.Data.OleDb;

namespace VodokanalReporter
{
	class Program
	{
		private static IMessageClass _messageClass;
		private static IListClass _listClass;
		private static ITaskClass _taskClass;
		private static IWinCCOleDBReader _winCC;
		
		public static void Main(string[] args)
		{
			//TODO: убрать в финальном релизе
			Console.ReadKey(true);
			
			_messageClass 	= new MessageClass();
			
			try
			{
				_listClass 		= new ListClass();
				_taskClass 		= new TaskClass(args);
				_winCC 			= new WinCCOleDBReader();
				
				ManagerClass manager = new ManagerClass(_winCC, _taskClass, _listClass, _messageClass);
			}
			catch (OleDbException ex)
			{
				_messageClass.MessageError("Ошибка OleDB: " + ex.ToString());
			}
			catch (InvalidOperationException ex)
			{
				_messageClass.MessageError("Недопустимая операция: " + ex.ToString());
			}
			catch (Exception ex)
			{
				_messageClass.MessageError(ex.ToString());
			}
			
			//TODO: убрать в финальном релизе
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}