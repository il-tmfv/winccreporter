﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 03.04.2015
 * Time: 16:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using WinCCReportCreator;
using System.Linq;

namespace VodokanalReporter
{

	public sealed class ExcelWriterResources : ExcelWriterBase
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="objName">Имя объекта, для которого строится отчет</param>
		/// <param name="listClass">Объект, реализующий интерфейс IListClass, предоставляющий список точек системы</param>
		/// <param name="taskClass">Объект, реализующий интерфейс ITaskClass, предоставляющий задание для построения отчета</param>
		/// <param name="winCC">Объект, реализующий интерфейс IWinCCOleDBReader, предоставляющий доступ к базе данных WinCC</param>
		public ExcelWriterResources(string objName, IListClass listClass, ITaskClass taskClass, IWinCCOleDBReader winCC) :
			base(objName, listClass, taskClass, winCC)
		{
	
		}
		
		/// <summary>
		/// Создание суммарного листа для отчета по ресурсам
		/// </summary>
		public void CreateSumPageReportResource()
		{
			int offset = 0;		
			
			ExcelWorksheet worksheet = _Excel.Workbook.Worksheets.Add("Суммарно");
			
			worksheet.Cells["A1"].Value = "Отчет за период: " + _taskClass.StartDate.ToString("dd.MM.yyyy HH:mm:ss") + " по " + _taskClass.EndDate.ToString("dd.MM.yyyy HH:mm:ss");
			
			worksheet.Cells["A3"].Value = "Расход воды";
			worksheet.Cells["A3"].Style.Font.Bold = true;
			worksheet.Cells["A3:E3"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

			worksheet.Cells["H3"].Value = "Расход электроэнергии";
			worksheet.Cells["H3"].Style.Font.Bold = true;
			worksheet.Cells["H3:L3"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
			
			TagHistoricalDataClass hdc;
			
			//цикл по всем точкам, которые принадлежат интересующему объекту
			//сперва смотрим расходы воды
			
			foreach (var point in _points)
			{
				if (point.Type == "F")
				{
					hdc = _winCC.ReadTagFromDB(
							"Archive", 
							point.TagName + "-total", 
							_taskClass.StartDate, 
							_taskClass.EndDate, "", 
							60, 
							TimeStepType.First);
					
					worksheet.Cells["A4"].Offset(offset, 0).Value = point.Code;
					worksheet.Cells["B4"].Offset(offset, 0).Value = point.Label;
					worksheet.Cells["E4"].Offset(offset, 0).Value = point.Unit;
					worksheet.Cells["D4"].Offset(offset, 0).Value = hdc.TotalSum;
					offset++;
				}					
			}
			
			if (offset == 0)
				worksheet.Cells["B4"].Offset(offset, 0).Value = "Отсутствуют сигналы данного типа";
			
			worksheet.Column(2).AutoFit();
			worksheet.Column(4).AutoFit();
			
			offset = 0;
			
			//теперь смотрим расходы электроэнергии
			foreach (var point in _points)
			{
				if (point.Type == "E")
				{
					hdc = _winCC.ReadTagFromDB(
							"Archive", 
							point.TagName + "-total", 
							_taskClass.StartDate, 
							_taskClass.EndDate, "", 
							60, 
							TimeStepType.First);
					
					worksheet.Cells["H4"].Offset(offset, 0).Value = point.Code;
					worksheet.Cells["I4"].Offset(offset, 0).Value = point.Label;
					worksheet.Cells["L4"].Offset(offset, 0).Value = point.Unit;
					worksheet.Cells["K4"].Offset(offset, 0).Value = hdc.TotalSum;
					offset++;
				}					
			}
			
			if (offset == 0)
				worksheet.Cells["I4"].Offset(offset, 0).Value = "Отсутствуют сигналы данного типа";
			
			worksheet.Column(9).AutoFit();
			worksheet.Column(11).AutoFit();
		}
		
		/// <summary>
		/// Функция проверяет, есть у объекта точки, по которым можно строить отчет, т.е. нужна ли отдельная страница
		/// </summary>
		/// <param name="resourceType">Тип ресурса, прописью в формате E F T P</param>
		/// <returns></returns>
		private bool IsPageNeeded(string resourceType)
		{
			var res = _points.Where(PointClass => PointClass.Type == resourceType).Count();
			if (res == 0)
				return false;
			else
				return true;
		}
		
		/// <summary>
		/// Создание листа с подробным отчетом по давлению воды
		/// </summary>
		/// <param name="resourceType">Тип ресурса в формате E F T P</param>
		/// <param name="resourceTypeName">Тип ресурса прописью, пойдет в название листа</param>
		public void CreatePageReportRecource(string resourceType, string resourceTypeName)
		{
			int offsetRow = 0, offsetColumn = 0;
			string formula = "";
			TagHistoricalDataClass hdc;
			int dayValuesCount = 1; //для запоминания количества точек, полученных за определенный день
			bool isDayEnded = false; //признак того, что день кончился и можно начинать группировку, вставлять формулы и графики
			bool isFirstDay = true; //признак того, что это первый день для параметра на листе
			int graphIndex = 0;
			DateTime previousDate;
			
			//не нашли точек для отображения
			if (!IsPageNeeded(resourceType))
				return;
			
			ExcelWorksheet worksheet = _Excel.Workbook.Worksheets.Add(resourceTypeName);
			
			foreach (var point in _points)
			{
				if (point.Type == resourceType)
				{
					hdc = _winCC.ReadTagFromDB(
						"Archive",
						point.TagName + "-value",
						_taskClass.StartDate,
						_taskClass.EndDate, "",
						3600,
						TimeStepType.Average);
					
					//TODO убрать, для теста
//					hdc = new TagHistoricalDataClass();
//					hdc.AddValue(1.0, DateTime.Now, 1, 1);
//					hdc.AddValue(2.0, DateTime.Now.AddMinutes(1), 2, 1);
//					hdc.AddValue(3.0, DateTime.Now.AddMinutes(2), 3, 1);
//					hdc.AddValue(3.2, DateTime.Now.AddMinutes(3), 3, 1);
//					hdc.AddValue(3.1, DateTime.Now.AddMinutes(4), 3, 1);
//					hdc.AddValue(3.3, DateTime.Now.AddMinutes(6), 3, 1);
//					hdc.AddValue(3.44, DateTime.Now.AddMinutes(8), 3, 1);
//					hdc.AddValue(1.5, DateTime.Now.AddMinutes(10), 3, 1);
//					
//					hdc.AddValue(4.0, DateTime.Now.AddDays(-1), 1, 1);
//					//hdc.AddValue(5.0, DateTime.Now.AddMinutes(1).AddDays(-1), 2, 1);
//					//hdc.AddValue(6.0, DateTime.Now.AddMinutes(2).AddDays(-1), 3, 1);
//					
//					hdc.AddValue(7.0, DateTime.Now.AddDays(-2), 1, 1);
//					hdc.AddValue(8.0, DateTime.Now.AddMinutes(1).AddDays(-2), 2, 1);
//					hdc.AddValue(9.1, DateTime.Now.AddMinutes(2).AddDays(-2), 3, 1);
//					hdc.AddValue(9.2, DateTime.Now.AddMinutes(3).AddDays(-2), 3, 1);
//					hdc.AddValue(9.3, DateTime.Now.AddMinutes(4).AddDays(-2), 3, 1);
//					hdc.AddValue(9.4, DateTime.Now.AddMinutes(5).AddDays(-2), 3, 1);
						
					previousDate = new DateTime(2000, 1, 1); //начальная дата, которой точно не будет в базе
					
					worksheet.Cells["A1"].Offset(offsetRow, offsetColumn).Value = point.Code;
					worksheet.Cells["B1"].Offset(offsetRow, offsetColumn).Value = point.Label;
					worksheet.Cells["D1"].Offset(offsetRow, offsetColumn).Value = point.Unit;
					
					worksheet.Cells["A1:D1"].Offset(offsetRow, offsetColumn).Style.Font.Bold = true;
					
					isDayEnded = false;
					isFirstDay = true;
					
					foreach (var val in hdc.TagValues)
					{
						if (val.TagTimeStamp.Date == previousDate.Date) //если день еще не кончился
						{
							worksheet.Cells["C2"].Offset(offsetRow, offsetColumn).Value = val.TagTimeStamp.ToString("dd.MM.yyyy HH:mm");
							worksheet.Cells["D2"].Offset(offsetRow, offsetColumn).Value = val.TagValue;
							worksheet.Cells["D2"].Offset(offsetRow, offsetColumn).Style.Numberformat.Format =  "0.00";
							dayValuesCount++;
						}
						else //наступил следующий день
						{
							worksheet.Cells["C2"].Offset(offsetRow, offsetColumn).Value = val.TagTimeStamp.ToString("dd.MM.yyyy");
							worksheet.Cells["C2:D2"].Offset(offsetRow, offsetColumn).Style.Font.Bold = true;
							worksheet.Cells["C2:D2"].Offset(offsetRow, offsetColumn).Style.Font.Italic = true;
							offsetRow++;
							worksheet.Cells["C2"].Offset(offsetRow, offsetColumn).Value = val.TagTimeStamp.ToString("dd.MM.yyyy HH:mm");
							worksheet.Cells["D2"].Offset(offsetRow, offsetColumn).Value = val.TagValue;
							worksheet.Cells["D2"].Offset(offsetRow, offsetColumn).Style.Numberformat.Format =  "0.00";
							
							if (isFirstDay)
								isFirstDay = false;
							else
								isDayEnded = true;
						}
						
						if (isDayEnded) //строим графики, группируем, вставляем формулы
						{
							//формула суммы
							formula = String.Format("(R{0}C{1}:R{2}C{1})",
							                        offsetRow - dayValuesCount + 1,
							                        4 + offsetColumn,
							                        offsetRow);
							worksheet.Cells["D2"].Offset(offsetRow - dayValuesCount - 2, offsetColumn).FormulaR1C1 = resourceType == "P" ? "=AVERAGE" + formula : "=SUM" + formula; //формула для подсчета
							worksheet.Cells["D2"].Offset(offsetRow - dayValuesCount - 2, offsetColumn).Style.Numberformat.Format =  "0.00";
							
							//группировка
							for (int i = offsetRow - dayValuesCount + 1; i <= offsetRow; i++)
								worksheet.Row(i).OutlineLevel = 1;
							
							//графики
							if (dayValuesCount > 5)
							{
								ExcelChart ec = worksheet.Drawings.AddChart("Line Time Graph " + graphIndex.ToString(), OfficeOpenXml.Drawing.Chart.eChartType.Line);
								graphIndex++;
								ec.Title.Text = "Часовые показания";
								ec.SetPosition(offsetRow - dayValuesCount, 0, offsetColumn + 5, 0);
								ec.SetSize(320, 20 * dayValuesCount);
								ec.Legend.Remove();
								
								string timeRangeAdr  = String.Format("C{0}:C{1}", offsetRow - dayValuesCount + 1, offsetRow);
								string valueRangeAdr = String.Format("D{0}:D{1}", offsetRow - dayValuesCount + 1, offsetRow);
								
								var timeRange  = worksheet.Cells[timeRangeAdr].Offset(0, offsetColumn);
								var valueRange = worksheet.Cells[valueRangeAdr].Offset(0, offsetColumn);
								
								var serie = ec.Series.Add(valueRange, timeRange);
							}
							
							dayValuesCount = 1;
							isDayEnded = false;
						}
						
						previousDate = val.TagTimeStamp;
						offsetRow++;
					}
					
					//Для того, чтобы последний день показать
					//***************************************
					//формула суммы
					formula = String.Format("(R{0}C{1}:R{2}C{1})",
					                        offsetRow - dayValuesCount + 2,
					                        4 + offsetColumn,
					                        offsetRow + 1);
					worksheet.Cells["D1"].Offset(offsetRow - dayValuesCount, offsetColumn).FormulaR1C1 = resourceType == "P" ? "=AVERAGE" + formula : "=SUM" + formula; //формула для подсчета
					worksheet.Cells["D1"].Offset(offsetRow - dayValuesCount, offsetColumn).Style.Numberformat.Format =  "0.00";
					
					//группировка
					for (int i = offsetRow - dayValuesCount + 2; i <= offsetRow + 1; i++)
						worksheet.Row(i).OutlineLevel = 1;
					
					//графики
					if (dayValuesCount > 5)
					{
						ExcelChart ec = worksheet.Drawings.AddChart("Line Time Graph " + graphIndex.ToString(), OfficeOpenXml.Drawing.Chart.eChartType.Line);
						graphIndex++;
						ec.Title.Text = "Часовые показания";
						ec.SetPosition(offsetRow - dayValuesCount + 1, 0, offsetColumn + 5, 0);
						ec.SetSize(320, 20 * dayValuesCount);
						ec.Legend.Remove();
						
						string timeRangeAdr  = String.Format("C{0}:C{1}", offsetRow - dayValuesCount + 2, offsetRow + 1);
						string valueRangeAdr = String.Format("D{0}:D{1}", offsetRow - dayValuesCount + 2, offsetRow + 1);
						
						var timeRange  = worksheet.Cells[timeRangeAdr].Offset(0, offsetColumn);
						var valueRange = worksheet.Cells[valueRangeAdr].Offset(0, offsetColumn);
						
						var serie = ec.Series.Add(valueRange, timeRange);
					}
					//***************************************
					
					worksheet.Column(1 + offsetColumn).AutoFit();
					worksheet.Column(2 + offsetColumn).AutoFit();
					worksheet.Column(3 + offsetColumn).AutoFit();
					worksheet.Column(4 + offsetColumn).AutoFit();
					worksheet.Column(5 + offsetColumn).AutoFit();
					
					dayValuesCount = 1;
					offsetRow = 0;
					offsetColumn += 10;
				}
			}
		}
	}
}
