﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 20.02.2015
 * Time: 8:59
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Diagnostics;
using NLog;

namespace VodokanalReporter
{
	public interface IMessageClass
	{
		void MessageSuccess(string sEvent);
		void MessageError(string sEvent);
	}
	
	/// <summary>
	/// Класс для вывода сообщений в лог сообщений
	/// </summary>
	public class MessageClass : IMessageClass
	{
		//private string sSource = "VodokanalReporter";
		//private string sLog = "Application";
		private Logger _logger;
		
		public void MessageSuccess(string sEvent)
		{
			_logger.Info(sEvent);
		}
		
		public void MessageError(string sEvent)
		{
			
			_logger.Error(sEvent);
		}
		
		public MessageClass()
		{
			_logger = LogManager.GetCurrentClassLogger();
		}
	}
}
