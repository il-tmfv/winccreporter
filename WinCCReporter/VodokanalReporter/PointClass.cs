﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 20.02.2015
 * Time: 8:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace VodokanalReporter
{
	/// <summary>
	/// Класс описывает одну точку, представляющую собой датчик
	/// </summary>
	public class PointClass
	{
		private string _tagName;
		private string _unit;
		private string _code;
		private string _label;
		private string _place;
		private bool   _IsTotal; //имеет ли данный папаметр поле Total
		
		/// <summary>
		/// Имя тега без суффикса
		/// </summary>
		public string TagName 
		{
			get { return _tagName; }
			private set { _tagName = value; }
		}
		
		public string Unit 
		{
			get { return _unit; }
			private set { _unit = value; }
		}

		public string Code 
		{
			get { return _code; }
			private set { _code = value; }
		}

		public string Label 
		{
			get { return _label; }
			private set { _label = value; }
		}

		public string Place 
		{
			get { return _place; }
			private set { _place = value; }
		}

		public string Type //E, F, P, T
		{
			get 
			{ 
				if (_code != null)
					if (_code.Length >= 1)
						return _code[0].ToString();
				
				return "";
			}
		}

		public bool IsTotal 
		{
			get { return _IsTotal; }
			private set { _IsTotal = value; }
		}
		
		public PointClass(string tagName, string code, string label, string unit, string place, bool isTotal)
		{
			TagName = tagName;
			Code = code;
			Label = label;
			Unit = unit;
			Place = place;
			IsTotal = isTotal;
		}
	}
}
