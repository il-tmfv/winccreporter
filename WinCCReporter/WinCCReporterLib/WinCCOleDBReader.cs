﻿/*
 * Created by SharpDevelop.
 * User: Admin
 * Date: 08.12.2014
 * Time: 17:25
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using System.Data.OleDb;

namespace WinCCReportCreator
{
	
	/// <summary>
	/// Тим выборки по времени
	/// </summary>
	public enum TimeStepType : int
	{
		None = 0,
		First = 1,
		Last = 2,
		Min = 3,
		Max = 4,
		Average = 5,
		Sum = 6,
		Count = 7,
		FirstInterpolated = 257,
		LastInterpolated = 258,
		MinInterpolated = 259,
		MaxInterpolated = 260,
		AverageInterpolated = 261,
		SumInterpolated = 262,
		CountInterpolated = 263
	}
	
	public interface IWinCCOleDBReader
	{
		TagHistoricalDataClass ReadTagFromDB(string archiveName, string tagName, DateTime startDateTime, DateTime endDateTime, string SQLClause, int timeStep, TimeStepType timeStepType);
		TagHistoricalDataClass ReadTagFromDB(string archiveName, string tagName, DateTime startDateTime, DateTime endDateTime);
		void OpenConnection(string CatalogName);
		void CloseConnection();
		bool IsConnected { get; }
	}
	
	/// <summary>
	/// Класс для работы с базой данных через OleDB
	/// </summary>
	public class WinCCOleDBReader : IWinCCOleDBReader
	{
		public WinCCOleDBReader()
		{
			_isConnected = false;
		}
		
		private OleDbConnection _myAccessConn = null;
		
		private bool _isConnected;
		
		public bool IsConnected
		{
			get 
			{
				return _isConnected; 
			}
		}
		
		/// <summary>
		/// Метод для корректировки времени (в базе используется время по UTC)
		/// </summary>
		/// <param name="dateTime">Время на локальной машине</param>
		/// <returns>Время в формате UTC</returns>
		private static DateTime LocalTimeToUTC(DateTime dateTime)
		{
			TimeSpan ts = TimeZoneInfo.Local.GetUtcOffset(dateTime);
			return (dateTime - ts);
		}
		
		/// <summary>
		/// Метод для перевода времени из UTC в локальное время
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		private static DateTime UTCToLocalTime(DateTime dateTime)
		{
			TimeSpan ts = TimeZoneInfo.Local.GetUtcOffset(dateTime);
			return (dateTime + ts);
		}

		/// <summary>
		/// Метод для построения строки подключения к базе
		/// </summary>
		/// <param name="archiveName"></param>
		/// <param name="tagName"></param>
		/// <param name="startDateTime"></param>
		/// <param name="endDateTime"></param>
		/// <param name="SQLClause"></param>
		/// <param name="timeStep"></param>
		/// <param name="timeStepType"></param>
		/// <returns></returns>
		private static string BuildQueryString(string archiveName, string tagName, DateTime startDateTime, DateTime endDateTime, string SQLClause, int timeStep, TimeStepType timeStepType)
		{
			DateTime dt1, dt2;
			
			dt1 = WinCCOleDBReader.LocalTimeToUTC(startDateTime);
			dt2 = WinCCOleDBReader.LocalTimeToUTC(endDateTime);
			
			string strAccessSelect = "";
			
			//корректируем неверно заданный интервал времени
			if (timeStep <= 0)
				timeStep = 1;
			
			//софрмировали строку запроса если нет запроса SQL и не применяется функция
			if (SQLClause == "" && timeStepType == TimeStepType.None)
				strAccessSelect = String.Format("TAG:R,'{0}\\{1}','{2}.000','{3}.000'", 
				                                       archiveName, 
				                                       tagName, 
				                                       dt1.ToString("yyyy-MM-dd HH:mm:ss"),
				                                       dt2.ToString("yyyy-MM-dd HH:mm:ss"));
			
			//софрмировали строку запроса если есть запрос SQL и не применяется функция
			if (SQLClause != "" && timeStepType == TimeStepType.None)
				strAccessSelect = String.Format("TAG:R,'{0}\\{1}','{2}.000','{3}.000','{4}'", 
				                                       archiveName, 
				                                       tagName, 
				                                       dt1.ToString("yyyy-MM-dd HH:mm:ss"),
				                                       dt2.ToString("yyyy-MM-dd HH:mm:ss"),
				                                       SQLClause);

			//софрмировали строку запроса если нет запроса SQL и применяется функция
			if (SQLClause == "" && timeStepType != TimeStepType.None)
				strAccessSelect = String.Format("TAG:R,'{0}\\{1}','{2}.000','{3}.000','TIMESTEP={4},{5}'", 
				                                       archiveName, 
				                                       tagName, 
				                                       dt1.ToString("yyyy-MM-dd HH:mm:ss"),
				                                       dt2.ToString("yyyy-MM-dd HH:mm:ss"),
				                                       timeStep.ToString(),
				                                       (int)timeStepType);
			
			//софрмировали строку запроса если есть запрос SQL и применяется функция
			if (SQLClause != "" && timeStepType != TimeStepType.None)
				strAccessSelect = String.Format("TAG:R,'{0}\\{1}','{2}.000','{3}.000','{4}','TIMESTEP={5},{6}'", 
				                                       archiveName, 
				                                       tagName, 
				                                       dt1.ToString("yyyy-MM-dd HH:mm:ss"),
				                                       dt2.ToString("yyyy-MM-dd HH:mm:ss"),
				                                       SQLClause,
				                                       timeStep.ToString(),
				                                       (int)timeStepType);
			
			return strAccessSelect;
		}
		
		/// <summary>
		/// Метод для чтения данных из базы данных WinCC
		/// </summary>
		/// <param name="archiveName"></param>
		/// <param name="tagName"></param>
		/// <param name="startDateTime"></param>
		/// <param name="endDateTime"></param>
		/// <param name="SQLClause"></param>
		/// <param name="timeStep"></param>
		/// <param name="timeStepType"></param>
		/// <returns></returns>
		public TagHistoricalDataClass ReadTagFromDB(string archiveName, string tagName, DateTime startDateTime, DateTime endDateTime, string SQLClause, int timeStep, TimeStepType timeStepType)
		{
			if (_isConnected == false)
				throw new InvalidOperationException("Сперва нужно подключиться к базе.");
			
			TagHistoricalDataClass hdc = new TagHistoricalDataClass();
						
			string strAccessSelect = BuildQueryString(archiveName, tagName, startDateTime, endDateTime, SQLClause, timeStep, timeStepType);
			
			DataSet myDataSet = new DataSet();
			OleDbCommand myAccessCommand = null;
			OleDbDataAdapter myDataAdapter = null;
			
			try
			{
				myAccessCommand = new OleDbCommand(strAccessSelect, _myAccessConn);
				myDataAdapter = new OleDbDataAdapter(myAccessCommand);
				myDataAdapter.Fill(myDataSet, archiveName);
					
	            //смотрим какие есть строки
	            DataRowCollection dra = myDataSet.Tables[archiveName].Rows;
	            foreach (DataRow dr in dra)
	            {
	            	hdc.AddValue((double)dr[2], UTCToLocalTime((DateTime)dr[1]), (int)dr[3], (int)dr[4]);
	            }
	            
	            return hdc;
			}
			catch
            {
                  throw;
            }
            finally
            {
            	if (myDataAdapter != null)
            		myDataAdapter.Dispose();
            	if (myDataSet != null)
            		myDataSet.Dispose();
            	if (myAccessCommand != null)
            		myAccessCommand.Dispose();
            }     
		}
		
		/// <summary>
		/// Метод для чтения данных из базы данных WinCC
		/// </summary>
		/// <param name="archiveName"></param>
		/// <param name="tagName"></param>
		/// <param name="startDateTime"></param>
		/// <param name="endDateTime"></param>
		/// <returns></returns>
		public TagHistoricalDataClass ReadTagFromDB(string archiveName, string tagName, DateTime startDateTime, DateTime endDateTime)
		{
			if (_isConnected == false)
				throw new InvalidOperationException("Сперва нужно подключиться к базе."); 
			
			TagHistoricalDataClass hdc = new TagHistoricalDataClass();
			
			DateTime dt1, dt2;
			
			dt1 = WinCCOleDBReader.LocalTimeToUTC(startDateTime);
			dt2 = WinCCOleDBReader.LocalTimeToUTC(endDateTime);
			
			//софрмировали строку запроса
			string strAccessSelect = String.Format("TAG:R,'{0}\\{1}','{2}.000','{3}.000'", 
			                                       archiveName, 
			                                       tagName, 
			                                       dt1.ToString("yyyy-MM-dd HH:mm:ss"),
			                                       dt2.ToString("yyyy-MM-dd HH:mm:ss"));
			
			DataSet myDataSet = new DataSet();
			OleDbCommand myAccessCommand = null;
			OleDbDataAdapter myDataAdapter = null;
			
			try
			{
				myAccessCommand = new OleDbCommand(strAccessSelect, _myAccessConn);
				myDataAdapter = new OleDbDataAdapter(myAccessCommand);
				myDataAdapter.Fill(myDataSet, archiveName);
				
				//смотрим какие есть строки
	            DataRowCollection dra = myDataSet.Tables[archiveName].Rows;
	            foreach (DataRow dr in dra)
	            {
	            	hdc.AddValue((double)dr[2], UTCToLocalTime((DateTime)dr[1]), (int)dr[3], (int)dr[4]);
	            }
	            
				return hdc;
			}
			catch
            {
                  throw;
            }
            finally
            {
            	if (myDataAdapter != null)
            		myDataAdapter.Dispose();
            	if (myDataSet != null)
            		myDataSet.Dispose();
            	if (myAccessCommand != null)
            		myAccessCommand.Dispose();
            }
		}
		
		/// <summary>
		/// Метод для открывает соединения с OLEDB провайдером WinCC
		/// </summary>
		/// <param name="CatalogName">Имя каталога WinCC (типа CC_testArc_14_12_07_12_11_43R)</param>
		public void OpenConnection(string CatalogName)
		{
			if (CatalogName.Length == 0)
				throw new Exception("Null \"Catalog\" field for connection string!");
			
			//формируем connestion string
			string strAccessConn = String.Format("Provider=WinCCOLEDBProvider.1;Catalog={1};Data Source={0}\\WinCC", 
			                                     Environment.MachineName, 
			                                     CatalogName);
			
			try
            {
                  _myAccessConn = new OleDbConnection(strAccessConn);
            }
            catch
            {
                _isConnected = false;  
            	throw;
            }
            _isConnected = true;
		}
		
		/// <summary>
		/// Метод для закрытия соединения с базой WinCC 
		/// </summary>
		public void CloseConnection()
		{
			if (_myAccessConn != null)
			{
				_myAccessConn.Close();
				_myAccessConn.Dispose();
				_isConnected = false;
			}
		}
	}
}
