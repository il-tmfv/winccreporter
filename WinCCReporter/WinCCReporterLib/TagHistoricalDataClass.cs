﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 08.12.2014
 * Time: 16:54
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace WinCCReportCreator
{
	/// <summary>
	/// Класс для хранения массива значений тега, полученных из TagLogging
	/// </summary>
	public class TagHistoricalDataClass
	{
		//для хранения массива значений, полученных из TL
		private List<TagValueClass> _tagValues;
		
		public TagHistoricalDataClass()
		{
			_tagValues = new List<TagValueClass>();
		}
		
		/// <summary>
		/// Возращает ссылку на коллекцию значений тегов
		/// </summary>
		public List<TagValueClass> TagValues
		{
			get
			{
				return _tagValues;
			}
		}
		
		/// <summary>
		/// Возвращает количество строк прочитанных данных из архива
		/// </summary>
		public int Count
		{
			get 
			{
				return _tagValues.Count;
			}
		}
		
		/// <summary>
		/// Добавляет значение в коллекцию
		/// </summary>
		/// <param name="tagValue">Значение для добавления</param>
		public void AddValue(TagValueClass tagValue)
		{
			_tagValues.Add(tagValue);
		}
		
		/// <summary>
		/// Возвращает суммарное значение параметра за весь интервал на основе накапливаемого значения
		/// </summary>
		public double TotalSum
		{
			get
			{
				double sum = 0.0;
				for (int i = 1; i < _tagValues.Count; i++)
				{
					if (_tagValues[i].TagValue >= _tagValues[i-1].TagValue)
						sum += _tagValues[i].TagValue - _tagValues[i-1].TagValue;
				}
				return sum;
			}
		}
		
		/// <summary>
		/// Добавляет значение в коллекцию
		/// </summary>
		/// <param name="tagValue">Значение тега</param>
		/// <param name="tagTimeStamp">Временной штамп тега</param>
		/// <param name="tagQuality">Качество тега</param>
		/// <param name="tagFlag">Флаги тега</param>
		public void AddValue(double tagValue, DateTime tagTimeStamp, int tagQuality, int tagFlag)
		{
			_tagValues.Add(new TagValueClass(tagValue, tagTimeStamp, tagQuality, tagFlag));
		}
	}
}
