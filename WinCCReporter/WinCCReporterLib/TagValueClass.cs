﻿/*
 * Created by SharpDevelop.
 * User: vpauser
 * Date: 08.12.2014
 * Time: 16:42
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace WinCCReportCreator
{
	/// <summary>
	/// Класс для хранения одной записи из TagLogging
	/// </summary>
	public class TagValueClass
	{
		private double tagValue;
		private DateTime tagTimeStamp;
		private int tagQuality;
		private int tagFlags;
		
		public double TagValue 
		{
			get 
			{
				return tagValue; 
			}
			set 
			{ 
				tagValue = value; 
			}
		}

		public DateTime TagTimeStamp 
		{
			get 
			{ 
				return tagTimeStamp; 
			}
			set 
			{ 
				tagTimeStamp = value; 
			}
		}

		public int TagQuality 
		{
			get 
			{ 
				return tagQuality; 
			}
			set 
			{ 
				tagQuality = value; 
			}
		}

		public int TagFlags 
		{
			get 
			{ 
				return tagFlags; 
			}
			set 
			{ 
				tagFlags = value; 
			}
		}
		
		/// <summary>
		/// конструктор с аргументами
		/// </summary>
		/// <param name="tagValue">Значение тега</param>
		/// <param name="tagTimeStamp">Врменной штамп тега</param>
		/// <param name="tagQuality">Качество тега</param>
		/// <param name="tagFlag">Флаг тега</param>
		public TagValueClass(double tagValue, DateTime tagTimeStamp, int tagQuality, int tagFlag)
		{
			TagFlags = tagFlag;
			TagQuality = tagQuality;
			TagTimeStamp = tagTimeStamp;
			TagValue = tagValue;
		}
		
		/// <summary>
		/// Пустой конструктор
		/// </summary>
		public TagValueClass()
		{
			
		}
	}
}
